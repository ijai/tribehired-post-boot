package my.amirul.tribehired.tribehiredpost.comment.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import my.amirul.tribehired.tribehiredpost.comment.model.Comment;

public interface CommentService {

	List<Comment> findAll();
	List<Comment> findByPost_Id(long id);
	List<Comment> findByNameContainingIgnoreCaseAndPost_IdOrEmailContainingIgnoreCaseAndPost_IdOrBodyContainingIgnoreCaseAndPost_Id
	(String name, long post_id1, String email, long post_id2, String body, long post_id3);
	
	List<Comment> findByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase
	(String name, String email, String body, Pageable pageable);
	
	Long countByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase
	(String name, String email, String body);
}
