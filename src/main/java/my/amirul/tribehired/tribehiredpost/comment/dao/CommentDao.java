package my.amirul.tribehired.tribehiredpost.comment.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import my.amirul.tribehired.tribehiredpost.comment.model.Comment;

@Transactional
public interface CommentDao extends JpaRepository<Comment, Long>{

	List<Comment> findAll();
	List<Comment> findByPost_Id(long id);
	List<Comment> findByNameContainingIgnoreCaseAndPost_IdOrEmailContainingIgnoreCaseAndPost_IdOrBodyContainingIgnoreCaseAndPost_Id
	(String name, long post_id1, String email, long post_id2, String body, long post_id3);
	
	List<Comment> findByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase
	(String name, String email, String body, Pageable pageable);
	
	Long countByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase
	(String name, String email, String body);
}
