package my.amirul.tribehired.tribehiredpost.comment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import my.amirul.tribehired.tribehiredpost.comment.dao.CommentDao;
import my.amirul.tribehired.tribehiredpost.comment.model.Comment;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	CommentDao _comment;
	
	@Override
	public List<Comment> findAll() {
		return _comment.findAll();
	}

	@Override
	public List<Comment> findByPost_Id(long id) {
		return _comment.findByPost_Id(id);
	}

	@Override
	public List<Comment> findByNameContainingIgnoreCaseAndPost_IdOrEmailContainingIgnoreCaseAndPost_IdOrBodyContainingIgnoreCaseAndPost_Id(
			String name, long post_id1, String email, long post_id2, String body, long post_id3) {
		return _comment.findByNameContainingIgnoreCaseAndPost_IdOrEmailContainingIgnoreCaseAndPost_IdOrBodyContainingIgnoreCaseAndPost_Id(name, post_id1, email, post_id2, body, post_id3);
	}

	@Override
	public List<Comment> findByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase(
			String name, String email, String body, Pageable pageable) {
		return _comment.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase(name, email, body, pageable);
	}

	@Override
	public Long countByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase(String name,
			String email, String body) {
		return _comment.countByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase(name, email, body);
	}


}
