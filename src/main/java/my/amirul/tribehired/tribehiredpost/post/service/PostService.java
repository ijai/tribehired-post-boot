package my.amirul.tribehired.tribehiredpost.post.service;

import java.util.List;

import my.amirul.tribehired.tribehiredpost.post.model.Post;

public interface PostService {

	List<Post> findAll();
	Post findById(long id);
	List<Post> findTopPost();
}
