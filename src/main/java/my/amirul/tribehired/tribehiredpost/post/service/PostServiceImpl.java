package my.amirul.tribehired.tribehiredpost.post.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import my.amirul.tribehired.tribehiredpost.post.dao.PostDao;
import my.amirul.tribehired.tribehiredpost.post.model.Post;

@Service
public class PostServiceImpl implements PostService{

	@Autowired
	PostDao _post;
	
	@Override
	public List<Post> findAll() {
		return _post.findAll();
	}

	@Override
	public Post findById(long id) {
		return _post.findById(id);
	}

	@Override
	public List<Post> findTopPost() {
		return _post.findTopPost();
	}


}
