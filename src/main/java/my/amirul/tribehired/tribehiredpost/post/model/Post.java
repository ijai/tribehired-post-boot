package my.amirul.tribehired.tribehiredpost.post.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import my.amirul.tribehired.tribehiredpost.comment.model.Comment;
import my.amirul.tribehired.tribehiredpost.user.model.User;

@Entity
@Table(name="post")
public class Post {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String title;
	private String body;
	
	@OneToOne
	@JsonIgnore
	private User user;
	
	@OneToMany(cascade = CascadeType.DETACH,
            fetch = FetchType.LAZY,
            mappedBy = "post")
	@JsonIgnore
    private Set<Comment> comments = new HashSet<>();
	
	@Transient
	private long userId;
	@Transient
	private long total_number_of_comments;
	
	public long getTotal_number_of_comments() {
		return this.comments.size();
	}

	public long getUserId() {
		return this.user.getId();
	}
	
	//Getter Setter
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Comment> getComments() {
		return comments;
	}
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	

}
