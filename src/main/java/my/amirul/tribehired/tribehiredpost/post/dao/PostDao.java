package my.amirul.tribehired.tribehiredpost.post.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import my.amirul.tribehired.tribehiredpost.post.model.Post;

@Transactional
public interface PostDao  extends JpaRepository<Post, Long>{
	
	List<Post> findAll();
	Post findById(long id);
	
	@Query(
	  value = "select *,(select COUNT(*) from main.comment cm where post.id = cm.post_id) comment_count " + 
	  		"from main.post post " + 
	  		"order by comment_count desc",
	  nativeQuery = true )
	List<Post> findTopPost();

}
