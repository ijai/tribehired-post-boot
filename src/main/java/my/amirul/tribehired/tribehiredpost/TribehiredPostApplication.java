package my.amirul.tribehired.tribehiredpost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TribehiredPostApplication {

	public static void main(String[] args) {
		SpringApplication.run(TribehiredPostApplication.class, args);
	}

}
