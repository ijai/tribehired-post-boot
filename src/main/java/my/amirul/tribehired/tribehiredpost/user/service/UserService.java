package my.amirul.tribehired.tribehiredpost.user.service;

import java.util.List;

import my.amirul.tribehired.tribehiredpost.user.model.User;

public interface UserService {

	List<User> findAll();
	User findById(long id);
}
