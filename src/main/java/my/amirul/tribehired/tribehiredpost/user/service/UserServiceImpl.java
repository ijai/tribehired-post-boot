package my.amirul.tribehired.tribehiredpost.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import my.amirul.tribehired.tribehiredpost.user.dao.UserDao;
import my.amirul.tribehired.tribehiredpost.user.model.User;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserDao _user;

	@Override
	public List<User> findAll() {
		return _user.findAll();
	}

	@Override
	public User findById(long id) {
		return _user.findById(id);
	}
	
}
