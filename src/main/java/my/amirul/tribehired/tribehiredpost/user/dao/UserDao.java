package my.amirul.tribehired.tribehiredpost.user.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import my.amirul.tribehired.tribehiredpost.user.model.User;

@Transactional
public interface UserDao extends JpaRepository<User, Long>{

	List<User> findAll();
	User findById(long id);
	
}

