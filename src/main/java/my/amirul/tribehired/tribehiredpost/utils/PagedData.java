package my.amirul.tribehired.tribehiredpost.utils;

import java.util.List;

public class PagedData<T> {
	
	private List<T> data;
	private int page;
	private int total_page;

	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotal_page() {
		return total_page;
	}
	public void setTotal_page(int total_page) {
		this.total_page = total_page;
	}

}