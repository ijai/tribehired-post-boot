package my.amirul.tribehired.tribehiredpost.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import my.amirul.tribehired.tribehiredpost.post.model.Post;
import my.amirul.tribehired.tribehiredpost.post.service.PostService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PostController {
	
	@Autowired
	PostService _post;

	
	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> getAllPosts() {
		
		List<Post> allpost = new ArrayList<Post>();
		allpost = _post.findTopPost();
		
		return new ResponseEntity<List<Post>>(allpost, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/posts/{post_id}", method = RequestMethod.GET)
	public ResponseEntity<Post> getSinglePost(@PathVariable("post_id") long post_id) {
		
		Post post = new Post();
		post = _post.findById(post_id);
		
		return new ResponseEntity<Post>(post, HttpStatus.OK);
	}

}
