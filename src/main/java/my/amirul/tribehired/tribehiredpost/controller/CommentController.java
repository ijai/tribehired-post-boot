package my.amirul.tribehired.tribehiredpost.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import my.amirul.tribehired.tribehiredpost.comment.model.Comment;
import my.amirul.tribehired.tribehiredpost.comment.service.CommentService;
import my.amirul.tribehired.tribehiredpost.post.service.PostService;
import my.amirul.tribehired.tribehiredpost.utils.PagedData;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CommentController {

	@Autowired
	PostService _post;
	
	@Autowired
	CommentService _comment;
	
	
	@RequestMapping(value = "/comments", method = RequestMethod.GET)
	public ResponseEntity<List<Comment>> getComments(
			@RequestParam(value="postId", required = false) Long postId,
			@RequestParam(value="searchTerm", required = false) String searchTerm
			) {
		
		List<Comment> allComments = new ArrayList<Comment>();
		if( postId == null ) {
			allComments = _comment.findAll();
		}else {
			if(searchTerm == null || searchTerm.equals(null)) {
				searchTerm = "";
			}
			allComments = _comment.findByNameContainingIgnoreCaseAndPost_IdOrEmailContainingIgnoreCaseAndPost_IdOrBodyContainingIgnoreCaseAndPost_Id
					(searchTerm, postId, searchTerm, postId, searchTerm, postId);
		}
		
		return new ResponseEntity<>(allComments, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/commentListing", method = RequestMethod.GET)
	public ResponseEntity<PagedData<Comment>> pageableComments(
			@RequestParam(value="sortBy", required = false) String sortBy,
			@RequestParam(value="sortDir", required = false) String sortDir,
			@RequestParam(value="search", required = false) String search,
			@RequestParam(value="page", required = false) Integer page,
			@RequestParam(value="per_page", required = false) Integer per_page
			) {
		Direction sortDirection = Direction.ASC;
		List<Comment> allComments = new ArrayList<Comment>();
		PagedData<Comment> pagedData = new PagedData<Comment>();
		
		if(sortBy == null || sortBy.equals(null)) {
			sortBy = "name";
		}
		
		if(search == null || search.equals(null)) {
			search = "";
		}
		
		if(sortDir == null || sortDir.equals(null)) {
			sortDirection = Direction.ASC;
		}else if(sortDir.toUpperCase().equals("DESC")) {
			sortDirection = Direction.DESC;
		}
		
		if(page == null) {
			page = 0;
		}
		if(per_page == null) {
			per_page = 10;
		}
		
		
		
		Sort sortObj = Sort.by(new Sort.Order(sortDirection, sortBy));
		Pageable pageable = PageRequest.of(page, per_page, sortObj);
		
		allComments = _comment.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase(search, search, search, pageable);
		Long totalL = _comment.countByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrBodyContainingIgnoreCase(search, search, search);
		
		int total = totalL.intValue();
		int totalPages = (int) Math.ceil(total / per_page);
		
		pagedData.setTotal_page(totalPages);
		pagedData.setPage(page);
		pagedData.setData(allComments);
		
		
		return new ResponseEntity<PagedData<Comment>>(pagedData, HttpStatus.OK);
	}
	
}
