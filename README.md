# TribehiredPost Spring Boot

This project was generated with [Spring Boot] version 2.4.0.

## Requirements: 

- postgresql [ 'tribehired' database name && 'main' schema ]

## REST api Endpoints:

-- /api/comments
parameters(not required):
* postId
* searchTerm

    

-- /api/commentListing 
parameters
* sortBy
* sortDir
* search
* page
* per_page


-- /api/posts
    
-- /api/posts/{post_id}



